<?php

/**
 * Implement an argument handler which loads both the terms and the
 * descendent terms for a loaded node.
 */
class views_related_nodes_plugin_argument_default_taxonomy_tid_descendent extends views_plugin_argument_default_taxonomy_tid {

  function get_argument() {
    $version = (int) views_api_version();
    if ($version === 2) {
       $arg = $this->parent_get_argument();
    } else {
       $arg = parent::get_argument();
    }

    $args = explode(',', $arg);
    $tids = $this->get_all_term_children($args);
    return implode('+', $tids);
  }

  function get_all_term_children($tids) {
    $childtids = array();
    while ($tid = array_pop($tids)) {
      $children = array_keys(taxonomy_get_children($tid));
      $tids = array_merge($tids, $children);
      $childtids[] = $tid;
    }
    return $childtids;
  }

  /**
   * Views 2.x is not capable of displaying the dependent options form and
   * saving the values correctly, so we simply monkey patch over the top
   * to remove the conditionals.
   */
  function parent_get_argument() {
    foreach (range(1, 3) as $i) {
      $node = menu_get_object('node', $i);
      if (!empty($node)) {
        break;
      }
    }
    // Just check, if a node could be detected.
    if ($node) {
      if (!empty($this->options['limit'])) {
        $tids = array();
        // filter by vid
        foreach ($node->taxonomy as $tid => $term) {
          if (!empty($this->options['vids'][$term->vid])) {
            $tids[] = $tid;
          }
        }
        return implode(",", $tids);
      }
      // Return all tids.
      else {
        return implode(",", array_keys($node->taxonomy));
      }
    }
  }
}
